## To create tarball for submission
ant -buildfile src/build.xml tarzip

CS542 Design Patterns
Spring 2016
PROJECT 4 README FILE

Due Date: Tuesday, April 19th, 2016
Submission Date: Tuesday, April 19th, 2016
Grace Period Used This Project: 0 Days
Grace Period Remaining: 3 Days
Author(s): Gabrielle Winterton and Kevin Travers
e-mail(s): gwinter1@binghamton.edu and ktraver1@binghamton.edu

PURPOSE:

[
The purpose of this assignment is use the visitor pattern to populate a tree with words read from an input file. 
This is done with the populateTreeVisitor which reads lines in from the file and puts them into the BST. 
Then, the wordCountVisitor goes though the BST to find the total amount of words, total amount of non-whitespace characters, and the most frequent word and its frequancy in the tree.
The GrepVisitor visotr then searches throughout the tree to search for word specified by the user. 
This whole process is performed in a lopp x amount of times where x is determned by the user. 
The time it takes on average for the program to complete a loop is then printed to the terminal, and the rest is outputed to the outputfile during the looping process.

]

PERCENT COMPLETE:

[
 We believe that we have completed 100% of this project.
]

PARTS THAT ARE NOT COMPLETE:

[
  We believe that we completed 100% of this project.
]

BUGS:

[
  None
]

FILES:

[
  Included with this project are 10 files:

  Driver.java, the main file of the program that contains main
  FileProcessor.java, the file that reads an input file one line at a time
  build.xml, the file that assists in compiling and running the code
  README.txt, the text file you are presently reading
  Node.java, the nodes of the tree which contains the string word and the freq of occurences
  BST.java, data strcuture we used which is BST
  populateTreeVisitor.java, Visiotr that visits the BST and populates the BST with words
  wordCountVisitor.java, visotr that visits the BS to find out the total amount of words
  treeProcessingVisitorI.java, the interface for the visitors
  grepVisitor.java, visits the bst to search for a specified word
  
  

  
]

SAMPLE OUTPUT:

[
  [java] timer: 4

  output.txt would now contain
The total number of words is: 310
The frequency of the most frequently used word is: 14
The most frequently used word is: the
The number of characters (without whitespaces) is: 1164
The word the occurs the following times: 14
The total number of words is: 310
The frequency of the most frequently used word is: 14
The most frequently used word is: the
The number of characters (without whitespaces) is: 1164
The word the occurs the following times: 14
The total number of words is: 310
The frequency of the most frequently used word is: 14
The most frequently used word is: the
The number of characters (without whitespaces) is: 1164
The word the occurs the following times: 14
The total number of words is: 310
The frequency of the most frequently used word is: 14
The most frequently used word is: the
The number of characters (without whitespaces) is: 1164
The word the occurs the following times: 14
The total number of words is: 310
The frequency of the most frequently used word is: 14
The most frequently used word is: the
The number of characters (without whitespaces) is: 1164
The word the occurs the following times: 14
]

TO COMPILE:

[
  Just extract the files and then do a "make". 
  Assuming you are in the directory containing this README:
  ant -buildfile src/build.xml all
]

TO RUN:

[
  Please run as: ant -buildfile src/build.xml run <INPUT FILE> <OUTPUT FILE> <NUM_ITERATION> <SEARCH_STRING>
  For example:   ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=output.txt -Darg2=20 -Darg3=the
]

EXTRA CREDIT:

[
  N/A
]


BIBLIOGRAPHY:

This serves as evidence that we are in no way intending Academic Dishonesty.
Gabrielle Winterton and Kevin Travers

[
  No outside resources were used
]

ACKNOWLEDGEMENT:

[

  During the coding process we did not talk to anyone else about the project

]
