package wordCount.util;

import java.io.BufferedReader;
import java.io.IOException;
//method String readLineFromFile();


public class FileProcessor
{
	/*
	Returns one line in the form of a string of the file it has been passed 
	@param   br  the Buffered Reader used to read the file's line
	@return      a string containing the line of the file
	*/
	public String readLineFromFile(BufferedReader br) throws IOException
	{
		
		String result = "";	
	try{
		result = br.readLine();
		
		
	}catch (Exception e) {
		System.err.println("Exception: "+e);
		System.exit(0);
	}
	return result;
	}

	
	
}
