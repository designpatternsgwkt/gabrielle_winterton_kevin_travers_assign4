
package wordCount.driver;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
//neded for the writer jazz
import java.io.FileWriter; //
import wordCount.util.FileProcessor;
import wordCount.visitors.PopulateTreeVisitor;
import wordCount.visitors.GrepVisitor;
import wordCount.visitors.WordCountVisitor;
import wordCount.visitors.TreeProcessingVisitorI;
import wordCount.treesForStrings.BST;

public class Driver{

	
	public static void main(String[] args){
		try{
		


		//do exceptions



		//int n = NUM_ITERATIONS

		//check for correct number of input from command line
			if (args[3].equals("${arg3}")){
				System.err.println("Not enough arguments: ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=output.txt -Darg2=20 -Darg3=the");
				System.exit(0);
			}else if(!args[4].equals("${arg4}")){
				System.err.println("Too many arguments: ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=output.txt -Darg2=20 -Darg3=the");
			System.exit(0);
			}
			//check for correct input from command line
		

			//open file
			File file = new File(args[0]);
			int NUM_ITERATIONS =  Integer.parseInt(args[2]);
			//check if is a file
			if(!file.isFile()){
				System.err.println("File entered does not exist");
				System.exit(0);
			}
			BufferedReader  br = new BufferedReader(new FileReader(file));
			FileWriter writer = new FileWriter(args[1]);
			//number of iterations
			//check if is an int
			
			//start timer
			long startTime = System.currentTimeMillis();
			for(int i = 0; i < NUM_ITERATIONS; i ++){
				//declare/instantiate the tree and visitors
				BST myTree = new BST();
				TreeProcessingVisitorI populateVisitor = new PopulateTreeVisitor(br);
				myTree.acceptVisitor(populateVisitor);
				//code to visit with the PopulateVisitor.
				
				//code to visit with the WordCountVisitor.
				TreeProcessingVisitorI wordCountVisitor = new WordCountVisitor(writer);
				myTree.acceptVisitor(wordCountVisitor);
	
				//code to grep with the grepVisitor.
				TreeProcessingVisitorI grepVisitor = new GrepVisitor(writer, args[3]);
				myTree.acceptVisitor(grepVisitor);


				br = new BufferedReader(new FileReader(file));
			}
			writer.close();
			//finish timer
			long endTime = System.currentTimeMillis();
			long totalTime = (endTime - startTime)/NUM_ITERATIONS;
			System.out.println("timer: "+totalTime);
			//Calculate total_time as (finishTime-startTime)/NUM_ITERATIONS.
			//Write the total_time value to stdout
	
		}catch(Exception e){
			System.err.println("Excption thrown:"+e);
			System.exit(0);
		}
	}
	



}


