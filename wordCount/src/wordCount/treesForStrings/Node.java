package wordCount.treesForStrings;



public class Node{
	//word
	//frequecny
	private String word;
	private int freq;
	private Node left;
	private Node right;

	public Node(String myWord){
		left = null;
		right = null;
		word = myWord;
		freq = 1;
		
	}


	public void setWord(String newWord){
		word = newWord;	
	}
	public String getWord(){
		return word;	
	}
	public void setFreq(int value){
		freq = value;
	}
	public int getFreq(){
		return freq;	
	}
	public void incFreq(){
		freq++;
	}
	public Node getRight(){
		return right;
	}
	public Node getLeft(){
		return left;
	}
	public void setLeft(Node newNode){
		left = newNode;
	}
	public void setRight(Node newNode){
		right = newNode;
	}
	
}
