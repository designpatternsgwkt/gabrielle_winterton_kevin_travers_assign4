
package wordCount.treesForStrings;

import wordCount.treesForStrings.Node;
import wordCount.visitors.TreeProcessingVisitorI;
import wordCount.treesForStrings.Node;
public class BST{
	
	private Node root;
	public void acceptVisitor(TreeProcessingVisitorI visitor){

		visitor.visit(this);

	}

	public BST(){
		root = null;
	}

	public Node insert(Node myRoot, String newWord){
		//once it hits a null then it is at its right spot cant have duplivcates (cant be inserted where a prevouis Node was inserted already)
		Node result = null;
//why dont you work
//come back latter to fix insert
//seems always fall into this if when it should only be calling it once for root

		if(isNodeEmpty(root)){
			
			
			root = new Node(newWord);
			return root;
			
		}else if(myRoot.getWord().compareTo(newWord) < 0){
			//go right cause value if bigger
			//if next is empty then we now that is where we want to insert else kep seeaching
			if(isNodeEmpty(myRoot.getRight())){
				Node newNode = new Node(newWord);
				myRoot.setRight(newNode);
				result = newNode;		
			}else{
				result = insert(myRoot.getRight(),newWord);
			}
			
		}else if(myRoot.getWord().compareTo(newWord) > 0){
			//got left becasue value is smaller
//if next is empty then we now that is where we want to insert else kep seeaching
			if(isNodeEmpty(myRoot.getLeft())){
			
				Node newNode = new Node(newWord);
				myRoot.setLeft(newNode);
				result = newNode;		
			}else{
				result = insert(myRoot.getLeft(),newWord);
			}
		}else{
			myRoot.incFreq();
			
			
		}
		return result;
	}

	public Boolean isNodeEmpty(Node myNode){
		
		Boolean result = true;
		if(myNode != null){
			result = false;
		}
		return result;
	}

	public Node getRoot(){
		return root;
	}
	
}
