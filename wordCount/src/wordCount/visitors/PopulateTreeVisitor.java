
package wordCount.visitors;
import wordCount.treesForStrings.BST;
import wordCount.treesForStrings.Node;
import wordCount.util.FileProcessor;
import java.io.BufferedReader;
import java.lang.String;
public class PopulateTreeVisitor implements TreeProcessingVisitorI{
	private BufferedReader brFile;

	public void visit(BST tree){//takes a tree

		//
		try{
			FileProcessor processor = new FileProcessor();
			String line = processor.readLineFromFile(getFile());

			String word;
			while(line != null){
				//create tree
				String[] words = line.split(" ");
				
				for(int i = 0; i<words.length;i++){
					if(words[i].trim().length() >0){
						
						Node newNode = tree.insert(tree.getRoot(),words[i]);
					}
					
				
				}
				
				
				line = processor.readLineFromFile(getFile());
			}
			
		}
		catch(Exception e){
			System.err.println("Excption thrown:"+e);
			System.exit(0);
		}
		

	}
	private BufferedReader getFile(){
		return brFile;
	}
	public PopulateTreeVisitor(BufferedReader file){
		brFile = file;
	}

}
