package wordCount.visitors;

import wordCount.treesForStrings.BST;
import wordCount.treesForStrings.Node;
import java.io.FileWriter;

public class GrepVisitor implements TreeProcessingVisitorI{

	private String word;
	private FileWriter writer;

	public GrepVisitor(FileWriter writerIn, String wordIn){

		writer = writerIn;
		word = wordIn;

	}	



	public void visit(BST tree){//takes a tree
		int freq = 0;		

		if(!tree.isNodeEmpty(tree.getRoot())){
			freq = visitHelper(tree.getRoot());
			//System.out.println("The word " + word + " occurs the following times: " + freq);
		}
		try{
			writer.write("The word " + word + " occurs the following times: " + freq + "\n");
		}
		catch(Exception e){
			System.err.println("Exception thrown: " + e);
			System.exit(0);
		}

		//Statements that will be written to output.txt when complete:
		//The word < search-string > occurs the following times: MMMN

	}


	private int visitHelper(Node curr){
		int freq = 0;
		if(curr.getWord().compareTo(word) == 0){
			//found match!
			freq = curr.getFreq();
		}
		else{

			if(curr.getWord().compareTo(word) < 0){
				if(curr.getRight() != null){
					freq = visitHelper(curr.getRight());
				}
			}
			else if(curr.getWord().compareTo(word) > 0){
				if(curr.getLeft() != null){
					freq = visitHelper(curr.getLeft());
				}
			}

		}
		return freq;
	}


}
