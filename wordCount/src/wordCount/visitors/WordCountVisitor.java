package wordCount.visitors;

import java.util.Vector;
import wordCount.treesForStrings.BST;
import wordCount.treesForStrings.Node;
import java.io.FileWriter;


public class WordCountVisitor implements TreeProcessingVisitorI{

	private FileWriter writer;
	private int totalWordCount;
	private int totalCharCount;
	private int maxFreq;
	private Vector <Node> mostFreqWord;

	public WordCountVisitor(FileWriter writerIn){
		writer = writerIn;
	}

	public void visit(BST tree){//takes a tree

		totalWordCount = 0;
		totalCharCount = 0;
		mostFreqWord = new Vector<Node>();
		maxFreq = 0;
		
		if(!tree.isNodeEmpty(tree.getRoot())){
			visitHelper(tree.getRoot());

			//System.out.println("The total number of words is: " + totalWordCount);
			//System.out.println("The frequency of the most frequently used word is: " + maxFreq);
			for(int i = 0; i < mostFreqWord.size(); i ++){
				//System.out.println("The most frequently used word is: " + mostFreqWord.elementAt(i).getWord());
			}
			//System.out.println("The number of characters (without whitespaces) is: " + totalCharCount);
		}
		
		try{
			writer.write("The total number of words is: " + totalWordCount + "\n");
			writer.write("The frequency of the most frequently used word is: " + maxFreq + "\n");
			for(int i = 0; i < mostFreqWord.size(); i ++){
				writer.write("The most frequently used word is: " + mostFreqWord.elementAt(i).getWord() + "\n");
			}
			writer.write("The number of characters (without whitespaces) is: " + totalCharCount + "\n");
		}
		catch(Exception e){
			System.err.println("Exception thrown: " + e);
			System.exit(0);
		}


		//Statements that will be written to output.txt when complete:
		//The total number of words is: NMNMN
		//The frequency of the most frequently used word is: NNNN
	//if the vector only has one word in it:
		//The most frequently used word is: ABC
	//else go through a loop to say which words are the max words
		//The number of characters (without whitespaces) is: MMMMM

	}


	private void visitHelper(Node curr){


		//Depth First Search on BST - use helper
			//for each node
			//totalWordCount += Node.wordCount
			//totalCharCount += (Node.word.size() * Node.wordCount)
			//if Node.wordCount > MaxFreq
				//clear vector and add Node to vector
				//make MaxFreq = Node.wordCount
			//else if Node.wordCount == MaxFreq
				//add Node to Vector
			//else move on


		
		totalWordCount += curr.getFreq();
		totalCharCount += (curr.getWord().length() * curr.getFreq());
		if(curr.getFreq() > maxFreq){
			mostFreqWord.removeAllElements();
			mostFreqWord.add(curr);
			maxFreq = curr.getFreq();
		}
		else if(curr.getFreq() == maxFreq){
			mostFreqWord.add(curr);
		}

		if(curr.getRight() != null){
			visitHelper(curr.getRight());
		}

		if(curr.getLeft() != null){
			visitHelper(curr.getLeft());
		}

		

	}









}
