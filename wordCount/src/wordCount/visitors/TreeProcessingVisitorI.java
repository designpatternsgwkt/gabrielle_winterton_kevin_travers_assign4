
package wordCount.visitors;
import wordCount.treesForStrings.BST;

public interface TreeProcessingVisitorI{

	public void visit(BST tree); //parameter will be the tree

}
